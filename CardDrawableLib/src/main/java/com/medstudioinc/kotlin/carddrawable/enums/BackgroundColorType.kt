package com.medstudioinc.kotlin.carddrawable.enums

enum class BackgroundColorType {
    SOLID,
    GRADIENT
}
