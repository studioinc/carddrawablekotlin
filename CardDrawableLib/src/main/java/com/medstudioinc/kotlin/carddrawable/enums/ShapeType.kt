package com.medstudioinc.kotlin.carddrawable.enums

enum class ShapeType {
    RECTANGLE,
    OVAL,
    LINE,
    RING
}