package com.medstudioinc.kotlin.carddrawable.utils

import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Build

object DrawUtils {

    fun getBackgroundDrawable(pressedColor: Int, backgroundDrawable: Drawable): RippleDrawable? {
        var rippleDrawable: RippleDrawable? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rippleDrawable = RippleDrawable(getPressedState(pressedColor), backgroundDrawable, null)
        }
        return rippleDrawable
    }

    fun getPressedState(pressedColor: Int): ColorStateList {
        return ColorStateList(arrayOf(intArrayOf()), intArrayOf(pressedColor))
    }

    //StateListDrawable instead selector
    fun getSelector(normalDrawable: Drawable, pressDrawable: Drawable): StateListDrawable {
        val stateListDrawable = StateListDrawable()
        stateListDrawable.addState(intArrayOf(android.R.attr.state_enabled, android.R.attr.state_pressed), pressDrawable)
        stateListDrawable.addState(intArrayOf(android.R.attr.state_enabled), normalDrawable)

        // Set the default state
        stateListDrawable.addState(intArrayOf(), normalDrawable)
        return stateListDrawable
    }
}
