package com.medstudioinc.carddrawable

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView

import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    internal lateinit var rv: RecyclerView
    internal lateinit var cardAdapter: CardLayerAdapter
    internal var cardModels = ArrayList<CardModel>()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv = findViewById(R.id.rv) as RecyclerView

        cardAdapter = CardLayerAdapter()

        rv.layoutManager = GridLayoutManager(this, 2)
        rv.adapter = cardAdapter

        cardModels.add(CardModel(0, R.drawable.ic_android_black_24dp, "Quotes", arrayOf("#A5CC82", "#00467F")))
        cardModels.add(CardModel(1, R.drawable.ic_android_black_24dp, "Flags", arrayOf("#FF0099", "#493240")))
        cardModels.add(CardModel(2, R.drawable.ic_android_black_24dp, "Favorite", arrayOf("#f5af19", "#f12711")))
        cardModels.add(CardModel(3, R.drawable.ic_android_black_24dp, "Calender", arrayOf("#ACBB78", "#799F0C")))
        cardModels.add(CardModel(4, R.drawable.ic_android_black_24dp, "Lifestyle", arrayOf("#ffd452", "#544a7d")))
        cardModels.add(CardModel(5, R.drawable.ic_android_black_24dp, "People", arrayOf("#F7BB97", "#DD5E89")))
        cardModels.add(CardModel(6, R.drawable.ic_android_black_24dp, "Picture", arrayOf("#35beb2", "#136a8a")))
        cardModels.add(CardModel(7, R.drawable.ic_android_black_24dp, "Music", arrayOf("#12c2e9", "#c471ed")))

        cardAdapter.addCards(cardModels)
    }
}
