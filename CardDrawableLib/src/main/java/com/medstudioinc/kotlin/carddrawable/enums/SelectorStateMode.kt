package com.medstudioinc.kotlin.carddrawable.enums

enum class SelectorStateMode {
    NORMAL,
    PRESSED
}
