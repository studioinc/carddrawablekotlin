package com.medstudioinc.kotlin.carddrawable.enums

enum class OrientationType {
    TOP_BOTTOM,
    TR_BL,
    RIGHT_LEFT,
    BR_TL,
    BOTTOM_TOP,
    BL_TR,
    LEFT_RIGHT,
    TL_BR
}