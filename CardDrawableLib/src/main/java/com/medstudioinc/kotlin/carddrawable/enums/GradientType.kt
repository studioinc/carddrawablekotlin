package com.medstudioinc.kotlin.carddrawable.enums

enum class GradientType {
    LINEAR_GRADIENT,
    RADIAL_GRADIENT,
    SWEEP_GRADIENT
}