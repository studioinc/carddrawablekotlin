package com.medstudioinc.carddrawable

import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView


import com.medstudioinc.kotlin.carddrawable.CardLayerDrawable
import com.medstudioinc.kotlin.carddrawable.enums.BackgroundColorType
import com.medstudioinc.kotlin.carddrawable.enums.GradientType
import com.medstudioinc.kotlin.carddrawable.enums.OrientationType
import com.medstudioinc.kotlin.carddrawable.enums.ShapeType

import java.util.ArrayList
import java.util.Random

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
class CardLayerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder> {
    internal var cardModels = ArrayList<CardModel>()
    internal var shuffle = 1

    constructor() {
        val random = Random()
        shuffle = random.nextInt(2)
    }

    fun addCards(cardModels: ArrayList<CardModel>) {
        for (cardModel in cardModels) {
            add(cardModel)
        }
    }

    fun add(cardModel: CardModel) {
        cardModels.add(cardModel)
        notifyItemInserted(cardModels.size - 1)
    }

    fun colors(): Array<Array<String?>> {
        var arrayNormalColors = Array(itemCount) { arrayOfNulls<String>(2) }
        for (i in 0 until itemCount) {
            Log.d("NormalColors", cardModels.get(i).colors!![0] as String? + " : " + cardModels.get(i).colors!![1] as String?)
            for (x in 0 until arrayNormalColors[i].size){
                arrayNormalColors[i][x] = cardModels.get(i).colors!![x] as String?
            }
        }
        return arrayNormalColors
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return ItemHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.card_item, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val cardModel = cardModels[i]
        val holder = viewHolder as ItemHolder
        holder.cardTitle.text = cardModel.title
        holder.imageView.setImageResource(cardModel.icon)
        holder.bind(holder, i)
    }

    private fun first(array: Array<Array<String?>>, i: Int): String? {
        return array[i][0]
    }

    private fun second(array: Array<Array<String?>>, i: Int): String? {
        return array[i][1]
    }

    inner class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var linearCard: LinearLayout
        internal var imageView: ImageView
        internal var cardTitle: TextView

        init {
            linearCard = itemView.findViewById<View>(R.id.linearCard) as LinearLayout
            imageView = itemView.findViewById<View>(R.id.imageView) as ImageView
            cardTitle = itemView.findViewById<View>(R.id.cardTitle) as TextView

            itemView.setOnClickListener { }

        }

         fun bind(holder: ItemHolder, position: Int) {
            CardLayerDrawable.fit(itemView.context)
                    .setView(holder.linearCard)
                    .setBackgroundColorType(BackgroundColorType.GRADIENT)
                    .setNormalColors(intArrayOf(Color.parseColor(first(colors(), position)), //arrayNormalColors[position][0]
                            Color.parseColor(second(colors(), position)) //arrayNormalColors[position][1]
                    ))
                    .setPressedColors(intArrayOf(Color.parseColor(second(colors(), position)), //arrayPressedColors[position][1]
                            Color.parseColor(first(colors(), position)) //arrayPressedColors[position][0]
                    ))
                    .setGradientRadius(1f)
                    .setCornersRadius(0.333f)
                    .setGradientType(GradientType.LINEAR_GRADIENT)
                    .setOrientationType(OrientationType.RIGHT_LEFT)
                    .setShapeType(ShapeType.RECTANGLE)
                    .setStokeWidth(0.0f)
                    .setStokeColor(Color.parseColor("#F2F2F2"))
                    .setBackgroundColor(Color.parseColor(first(colors(), position)))
                    .setBorderSolidColor(Color.parseColor(second(colors(), position)))//d1d5da
                    .setBorderGradientColor(intArrayOf(Color.parseColor(second(colors(), position)), Color.parseColor(second(colors(), position))))//d1d5da
                    .setRippleColor(Color.parseColor(second(colors(), position)))
                    .setLeft(1)
                    .setTop(1)
                    .setRight(1)
                    .setBottom(10)
                    .draw()
        }
    }

    override fun getItemCount(): Int {
        return cardModels.size
    }
}
