package com.medstudioinc.carddrawable

class CardModel {
    internal var id: Int = 0
    internal var icon: Int = 0
    internal lateinit var title: String
    internal lateinit var colors: Array<Any>

    constructor() {}

    constructor(index: Int, title: String) {
        this.id = index
        this.title = title
    }

    constructor(index: Int, icon: Int, title: String) {
        this.id = index
        this.icon = icon
        this.title = title
    }

    constructor(index: Int, title: String, colors: Array<Any>) {
        this.id = index
        this.title = title
        this.colors = colors
    }

    constructor(index: Int, icon: Int, title: String, colors: Array<Any>) {
        this.id = index
        this.icon = icon
        this.title = title
        this.colors = colors
    }

     fun getId(): Int{
        return this.id
     }

     fun getIcon(): Int{
         return icon
     }

    fun getTitle(): String{
        return title
    }

    fun getColors(): Array<Any>{
        return colors
    }
}
