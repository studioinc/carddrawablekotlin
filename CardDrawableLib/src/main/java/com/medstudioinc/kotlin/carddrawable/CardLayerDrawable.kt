package com.medstudioinc.kotlin.carddrawable

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.RippleDrawable
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.View

import com.medstudioinc.kotlin.carddrawable.enums.OrientationType
import com.medstudioinc.kotlin.carddrawable.enums.SelectorStateMode
import com.medstudioinc.kotlin.carddrawable.enums.ShapeType
import com.medstudioinc.kotlin.carddrawable.utils.DisplayUtility
import com.medstudioinc.kotlin.carddrawable.utils.DrawUtils
import com.medstudioinc.kotlin.carddrawable.enums.BackgroundColorType
import com.medstudioinc.kotlin.carddrawable.enums.GradientType


/**
 * Author : Mohammmed Ajaroud
 * Birthday : 27/09/1992
 * Github : https://www.github.com/mohammedajaroud
 * Created At : 11/10/2018 - 13:15h PM
 * OS Version : Mac OS X Hight Sierra
 * Android Studio : 3.1.4
 * Target SDK : 28.0.0
 * Languages : Java/Kotlin
 */


@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
class CardLayerDrawable(private val context: Context) {
    private var viewGroup: View? = null

    private var backgroundColor: Int = 0
    private var borderSolidColor: Int = 0
    private var borderGradientColor: IntArray? = null
    private var rippleColor: Int = 0

    private var stokeColor: Int = 0
    private var stokeWidth: Int = 0
    private var normalColors: IntArray? = null
    private var pressedColors: IntArray? = null
    private var cornersRadius: Float = 0.toFloat()
    private var gradientRadius: Float = 0.toFloat()

    private var left: Int = 0
    private var top: Int = 0
    private var right: Int = 0
    private var bottom: Int = 0

    private var gradientType: GradientType? = null
    private var shapeType: ShapeType? = null
    private var orientationType: OrientationType? = null
    private var backgroundColorType: BackgroundColorType? = null
    private var selectorStateMode: SelectorStateMode? = null

    init {
        defaultValues()
    }

    private fun defaultValues() {
        this.backgroundColor = Color.parseColor("#FFFFFF")
        this.borderSolidColor = Color.parseColor("#69d8d8d8")
        this.borderGradientColor = intArrayOf(Color.parseColor("#69d8d8d8"), Color.parseColor("#EAEAEA"))
        this.rippleColor = Color.parseColor("#FFFFFF")
        this.stokeColor = Color.parseColor("#EAEAEA")

        this.stokeWidth = DisplayUtility.dp2px(context, 0f)

        this.normalColors = intArrayOf(Color.RED, Color.BLACK, Color.BLUE)
        this.pressedColors = intArrayOf(Color.GRAY, Color.GREEN, Color.DKGRAY)

        this.cornersRadius = 0f
        this.gradientRadius = 0f

        this.gradientType = GradientType.LINEAR_GRADIENT
        this.shapeType = ShapeType.RECTANGLE
        this.orientationType = OrientationType.TOP_BOTTOM
        this.backgroundColorType = BackgroundColorType.SOLID
        this.selectorStateMode = SelectorStateMode.NORMAL
    }

    fun setView(view: View): CardLayerDrawable {
        this.viewGroup = view
        return this
    }

    fun setBackgroundColorType(backgroundColorType: BackgroundColorType): CardLayerDrawable {
        this.backgroundColorType = backgroundColorType
        return this
    }

    fun setNormalColors(colors: IntArray): CardLayerDrawable {
        this.normalColors = colors
        return this
    }

    fun setPressedColors(colors: IntArray): CardLayerDrawable {
        this.pressedColors = colors
        return this
    }

    fun setBackgroundColor(color: Int): CardLayerDrawable {
        this.backgroundColor = color
        return this
    }

    fun setBorderSolidColor(color: Int): CardLayerDrawable {
        this.borderSolidColor = color
        return this
    }

    fun setBorderGradientColor(colors: IntArray): CardLayerDrawable {
        this.borderGradientColor = colors
        return this
    }

    fun setRippleColor(color: Int): CardLayerDrawable {
        this.rippleColor = color
        return this
    }

    fun setStokeColor(color: Int): CardLayerDrawable {
        this.stokeColor = color
        return this
    }

    fun setStokeWidth(width: Float): CardLayerDrawable {
        this.stokeWidth = DisplayUtility.dp2px(context, width)
        return this
    }

    fun setCornersRadius(cornersRadius: Float): CardLayerDrawable {
        this.cornersRadius = cornersRadius
        return this
    }

    fun setGradientRadius(gradientRadius: Float): CardLayerDrawable {
        this.gradientRadius = gradientRadius
        return this
    }

    fun setGradientType(gradientType: GradientType): CardLayerDrawable {
        this.gradientType = gradientType
        return this
    }

    fun setShapeType(shapeType: ShapeType): CardLayerDrawable {
        this.shapeType = shapeType
        return this
    }

    fun setOrientationType(orientationType: OrientationType): CardLayerDrawable {
        this.orientationType = orientationType
        return this
    }


    fun setLeft(left: Int): CardLayerDrawable {
        this.left = left
        return this
    }

    fun setTop(top: Int): CardLayerDrawable {
        this.top = top
        return this
    }

    fun setRight(right: Int): CardLayerDrawable {
        this.right = right
        return this
    }

    fun setBottom(bottom: Int): CardLayerDrawable {
        this.bottom = bottom
        return this
    }

    // draw
    fun draw() {
        // Set GradientDrawable as ImageView source image
        if (viewGroup != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val rippleDrawable = DrawUtils.getBackgroundDrawable(rippleColor, RippleDrawable(DrawUtils.getPressedState(rippleColor), DrawUtils.getSelector(
                        getDrawable(SelectorStateMode.NORMAL),
                        getDrawable(SelectorStateMode.NORMAL)), null))

                this.viewGroup!!.background = rippleDrawable
            } else {
                this.viewGroup!!.background = DrawUtils.getSelector(getDrawable(SelectorStateMode.NORMAL), getDrawable(SelectorStateMode.PRESSED))
            }
        }
    }


    //GradientDrawable instead Drawable
    fun getDrawable(selectorStateMode: SelectorStateMode): Drawable {
        this.selectorStateMode = selectorStateMode
        // Initialize a new GradientDrawable
        val background = GradientDrawable()
        val border = GradientDrawable()

        //Sets the radius of the gradient.
        background.gradientRadius = gradientRadius
        border.gradientRadius = gradientRadius

        //Sets the radius of the corner.
        background.cornerRadius = cornersRadius
        border.cornerRadius = cornersRadius


        // Set pixels width solid stoke color border
        background.setStroke(stokeWidth, stokeColor, 8f, 5f)//设置边框厚度和颜色
        border.setStroke(stokeWidth, stokeColor)//设置边框厚度和颜色


        // Set the color array to draw gradient
        setBackgroundColorType(arrayOf(background, border))

        // Set GradientDrawable shape is a rectangle
        setShape(arrayOf(background, border), this.shapeType!!)

        // Set the GradientDrawable orientation
        setOrientation(arrayOf(background, border), this.orientationType!!)

        // Set the GradientDrawable gradient type linear gradient
        setGradientType(arrayOf(background, border), this.gradientType!!)


        val layers = arrayOf<Drawable>(background, border)
        val layerDrawable = LayerDrawable(layers)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layerDrawable.setPadding(16, 8, 16, 8)
        }

        layerDrawable.setLayerInset(0, 0, 0, 0, 0)
        layerDrawable.setLayerInset(1, left, top, right, bottom)

        /*
           Sets the center location of the gradient.
           setGradientCenter(float x, float y)
           //background.setGradientCenter(0,0);
        */

        // Set GradientDrawable width and in pixels
        //background.setSize(200, 200); // Width 400 pixels and height 100 pixels
        return layerDrawable

    }

    private fun setOrientation(drawables: Array<GradientDrawable>, orientationType: OrientationType) {
        when (orientationType) {
            OrientationType.TOP_BOTTOM -> drawables[1].orientation = GradientDrawable.Orientation.TOP_BOTTOM
            OrientationType.TR_BL -> drawables[1].orientation = GradientDrawable.Orientation.TR_BL
            OrientationType.RIGHT_LEFT -> drawables[1].orientation = GradientDrawable.Orientation.RIGHT_LEFT
            OrientationType.BR_TL -> drawables[1].orientation = GradientDrawable.Orientation.BR_TL
            OrientationType.BOTTOM_TOP -> drawables[1].orientation = GradientDrawable.Orientation.BOTTOM_TOP
            OrientationType.LEFT_RIGHT -> drawables[1].orientation = GradientDrawable.Orientation.LEFT_RIGHT
            OrientationType.BL_TR -> drawables[1].orientation = GradientDrawable.Orientation.BL_TR
        }
    }

    private fun setGradientType(drawables: Array<GradientDrawable>, gradientType: GradientType) {
        when (gradientType) {
            GradientType.LINEAR_GRADIENT -> drawables[1].gradientType = GradientDrawable.LINEAR_GRADIENT
            GradientType.RADIAL_GRADIENT -> drawables[1].gradientType = GradientDrawable.RADIAL_GRADIENT
            GradientType.SWEEP_GRADIENT -> drawables[1].gradientType = GradientDrawable.SWEEP_GRADIENT
        }
    }

    private fun setBackgroundColorType(drawables: Array<GradientDrawable>) {
        when (this.backgroundColorType) {
            BackgroundColorType.SOLID -> {
                drawables[0].setColor(borderSolidColor)
                when (selectorStateMode) {
                    SelectorStateMode.NORMAL -> drawables[1].setColor(backgroundColor)
                    SelectorStateMode.PRESSED -> drawables[1].setColor(rippleColor)
                }
            }
            BackgroundColorType.GRADIENT -> {
                drawables[0].colors = borderGradientColor
                when (selectorStateMode) {
                    SelectorStateMode.NORMAL -> drawables[1].colors = normalColors
                    SelectorStateMode.PRESSED -> drawables[1].colors = pressedColors
                }
            }
        }
    }

    private fun setShape(drawables: Array<GradientDrawable>, shapeType: ShapeType) {
        when (shapeType) {
            ShapeType.RECTANGLE -> drawables[0].shape = GradientDrawable.RECTANGLE
            ShapeType.OVAL -> drawables[0].shape = GradientDrawable.OVAL
            ShapeType.LINE -> drawables[0].shape = GradientDrawable.LINE
            ShapeType.RING -> drawables[0].shape = GradientDrawable.RING
        }
    }

    companion object {

        fun fit(context: Context): CardLayerDrawable {
            return CardLayerDrawable(context)
        }
    }

}
