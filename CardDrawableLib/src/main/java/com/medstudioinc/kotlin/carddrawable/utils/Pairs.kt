package com.medstudioinc.kotlin.carddrawable.utils

class Pairs {
    var first: String? = null
    var second: String? = null

    constructor() {}

    constructor(first: String, second: String) {
        this.first = first
        this.second = second
    }

    companion object {

        fun with(): Pairs {
            return Pairs()
        }
    }

    fun first(first: String): Pairs {
        this.first = first
        return this
    }

    fun second(second: String): Pairs {
        this.second = second
        return this
    }
}
